local composer = require( "composer" )
 
local scene = composer.newScene()


local function irParaTelaPartida( self, event )
    irParaTelaExibicao()
end

function irParaTelaExibicao()
    composer.gotoScene( "vestiario-final", {effect = "fade", time = "100"} )
end  
 

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
 
end
 
 
-- show()
function scene:show( event )
    
    local sceneGroup = self.view
    local phase = event.phase
 
    audio.play( musica, { channel=1, loops=-1 } )
    audio.setVolume( 0.2, { channel=1 } )

    if ( phase == "will" ) then
        bg = display.newImage("images/exibicao_japao.png")
        bg.x = centerX
        bg.y = centerY
        
        play = display.newImage("images/btn_vestiario.png")
        play.x = centerX-500
        play.y = centerY+400

        transition.to( play, { time=1000, alpha=1.0, x=centerX+200, y=centerY+350} )
          
        play.touch = irParaTelaPartida
        play:addEventListener( "touch", play )
       

    elseif ( phase == "did" ) then
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        play:removeSelf()
        bg:removeSelf()
    elseif ( phase == "did" ) then
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene