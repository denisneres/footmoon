local composer = require( "composer" )
 
local scene = composer.newScene()


local function irParaTelaPartida( self, event )
    irParaTelaExibicao()
end

function irParaTelaExibicao()
    composer.gotoScene( "partida-final", {effect = "fade", time = "100", params={jogadores = jogadores } } )
end  
 

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
 
end
 
 
-- show()
function scene:show( event )
    

    audio.stop( { channel=1 })

    audio.play( torcida, { channel=2, loops=-1 } )
    audio.setVolume( 0.2, { channel=2 } )

    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then

        jogadores = event.params.jogadores

        bg = display.newImage("images/escalacao_japao.png")
        bg.x = centerX
        bg.y = centerY
        
        play = display.newImage("images/btn_comecar.png")
        play.x = centerX
        play.y = centerY+400

          
        play.touch = irParaTelaPartida
        play:addEventListener( "touch", play )
       

    elseif ( phase == "did" ) then
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        play:removeSelf()
        bg:removeSelf()
    elseif ( phase == "did" ) then
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene