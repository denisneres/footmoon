WORLD CUP MOON
=======================================================

 + **Nome do Jogo**: World Cup FootMoon
 
 + **Gênero**: Futebol
 
 + **Público Alvo**: Fãs de futebol de todas as idades
 
 + **Objetivo geral**: ser o treinador de um time na copa do mundo e vencer a copa.
 
 + **Objetivos específicos**: 
    * Téra que saber escalar um time ideal para a partida
    * saber fazer substituições cruciais para que vença a partida. 
 
 + **Enredo e personagens**: O enredo é uma copa do mundo de futebol, em que países se enfrentam em busca da taça. Os personagens são os demais times e seus representantes (jogadores)
 
 + **Personagem principal**: Jogadores de futebol da seleção que o jogador escolher 
 
 + **Inimigos**: Times adversários que terá que enfrentar
 
 + **Iteração do jogador**: Partidas que terá que ganhar até a grande FINAL
 
 + **Controles**: 
   * Mouse (cliques)
   * Teclado (Comandos de ataque e defesa do seu time)
