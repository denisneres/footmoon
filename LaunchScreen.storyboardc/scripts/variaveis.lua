local function addToScore(num)
    score = score + num
    scoreText.text = "Score: " .. tostring(score)
end

function ataqueMedia(time)
    local jogadores = time.jogadores
    ataque = 0
    ataque = ataque + jogadores[1].ataque + jogadores[2].ataque + jogadores[3].ataque + jogadores[4].ataque + jogadores[5].ataque
    return ataque/5
end

function defesaMedia(t)
    local jogadores = t.jogadores
    defesa = 0
    defesa = defesa + jogadores[1].defesa + jogadores[2].defesa + jogadores[3].defesa + jogadores[4].defesa + jogadores[5].defesa
    return defesa/5
end

local selecoes = {

    {name="Brasil", jogadores= { 
                        {name="Neymar", ataque="89", defesa="15"}, 
                        {name="Jesus", ataque="75", defesa="30"},
                        {name="Marcelo", ataque="71", defesa="55"},
                        {name="Daniel Alves", ataque="70", defesa="55"},
                        {name="Miranda", ataque="13", defesa="80"} 
                    } 
    },
    
    {name="Argentina", jogadores= { 
                            {name="Messi", ataque="95", defesa="10"}, 
                            {name="Aguero", ataque="78", defesa="18"},
                            {name="Di Maria", ataque="75", defesa="20"},
                            {name="Mascherano", ataque="20", defesa="75"},
                            {name="Otamendi", ataque="14", defesa="77"} 
                        } 
    },
    
    {name="Franca", jogadores= { 
                        {name="Mbappe", ataque="86", defesa="20"}, 
                        {name="Pogba", ataque="75", defesa="70"},
                        {name="Griezman", ataque="82", defesa="10"},
                        {name="Kante", ataque="70", defesa="75"},
                        {name="Varane", ataque="19", defesa="80"} 
                    } 
    },
    
    {name="Alemanha", jogadores= { 
                            {name="Kross", ataque="65", defesa="65"}, 
                            {name="Muller", ataque="80", defesa="20"},
                            {name="Ozil", ataque="75", defesa="30"},
                            {name="Gundogan", ataque="50", defesa="69"},
                            {name="Boateng", ataque="10", defesa="78"}
                        } 
    }
}

brasil = selecoes[1]
argentina = selecoes[2]
franca = selecoes[3]
alemanha = selecoes[4]

ataqueBrasil = ataqueMedia(brasil)
ataqueArgentina = ataqueMedia(argentina)
ataqueAlemanha = ataqueMedia(alemanha)
ataqueFranca = ataqueMedia(franca)


defArg = defesaMedia(brasil)
defBra = defesaMedia(argentina)
defFra = defesaMedia(franca)
defAle = defesaMedia(alemanha)

io.write("ataque brasil: " .. ataqueBrasil .. "\n")
io.write("ataque argentina: " .. ataqueArgentina .. "\n")
io.write("ataque franca: " .. ataqueFranca .. "\n")
io.write("ataque alemanha: " .. ataqueAlemanha .. "\n")
io.write("----------------------------------------\n")

io.write("defesa brasil: " .. defBra .. "\n")
io.write("defesa argentina: " .. defArg .. "\n")
io.write("defesa franca: " .. defFra .. "\n")
io.write("defesa alemanha: " .. defAle .. "\n")