local composer = require( "composer" )
 
local scene = composer.newScene()
local bg = display.newImage("images/vitoriabg.png")
local btComecar = display.newImage("images/vitoriabg_play.png")
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5

vitoria = audio.loadStream( "sounds/vitoria.wav" )


local function irParaTelaPartida( self, event )
        audio.play(click)
        irParaTelaSelecionarTimes()
end
    
function irParaTelaSelecionarTimes()
    composer.gotoScene( "exibicaoFinal", {effect = "fade", time = "400"} )
end  

function scene:create( event )
    audio.play(vitoria)
    local sceneGroup = self.view
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
    
 
    if ( phase == "will" ) then
        
        audio.play(vitoria)
        
        bg.x = centerX
        bg.y = centerY
 
        btComecar.x = centerX
        btComecar.y = centerY+300

        btComecar.touch = irParaTelaPartida
        btComecar:addEventListener( "touch", btComecar )
       
    elseif ( phase == "did" ) then
       
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        bg.isVisible = false
        bg:removeSelf()
        btComecar:removeSelf()
 
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene