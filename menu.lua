local composer = require( "composer" )
 
local scene = composer.newScene()
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5

musica = audio.loadStream( "sounds/menu_sound.wav" )

function selecionarTime(event)

end

options = {
    effect = "flip",
    time = 1000
}

local function irParaTelaPartida( self, event )
        irParaTelaSelecionarTimes()
end
    
function irParaTelaSelecionarTimes()
        composer.gotoScene( "introducao", {effect = "fade", time = "400"} )
end  

function scene:create( event )
    local sceneGroup = self.view
end

function scene:create( event )
    local sceneGroup = self.view
    
end
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
 
    if ( phase == "will" ) then

        bg = display.newImageRect("images/tela_menu.png",640,960)
        btComecar = display.newImageRect("images/botao_comecar.png",400,100)
        audio.play( musica, { channel=1, loops=-1 } )
        audio.setVolume( 0.2, { channel=1 } )

        bg.x = centerX
        bg.y = centerY
        btComecar.x = centerX
        btComecar.y = centerY+150

        btComecar.touch = irParaTelaPartida
        btComecar:addEventListener( "touch", btComecar )
       
    elseif ( phase == "did" ) then
       
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        bg:removeSelf()
        btComecar:removeSelf()
 
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene