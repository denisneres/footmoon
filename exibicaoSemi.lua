local composer = require( "composer" )
 
local scene = composer.newScene()


local function irParaTelaPartida( self, event )
    irParaTelaExibicao()
end

function irParaTelaExibicao()
    play:removeSelf()
    bg:removeSelf()

    composer.gotoScene( "vestiario-semi", {effect = "fade", time = "100"} )
end  
 

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
 
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then

        audio.play( musica, { channel=1, loops=-1 } )
        audio.setVolume( 0.2, { channel=1 } )

        bg = display.newImage("images/exibicao_italia.png")
        bg.x = centerX
        bg.y = centerY
        
        play = display.newImage("images/btn_vestiario.png")
        play.x = centerX+200
        play.y = centerY+400

          
        play.touch = irParaTelaPartida
        play:addEventListener( "touch", play )
       

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene