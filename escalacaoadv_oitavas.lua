local composer = require( "composer" )
 
local scene = composer.newScene()


torcida = audio.loadStream( "sounds/torcida.wav" )


local function irParaTelaPartida( self, event )
    audio.play(click)
    irParaTelaExibicao()
end

function irParaTelaExibicao()
    composer.gotoScene( "partida", {effect = "fade", time = "100", params={jogadores = jogadores } } )
end  
 

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
 
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase

 
    if ( phase == "will" ) then


        audio.stop( { channel=1 })

        audio.play( torcida, { channel=2, loops=-1 } )
        audio.setVolume( 0.2, { channel=2 } )

      
        jogadores = event.params.jogadores

        escalacaoImg = display.newImage("images/escalacaomexico.png")
        escalacaoImg.x = centerX
        escalacaoImg.y = centerY
        
        playbtn = display.newImage("images/btn_comecar.png")
        playbtn.x = centerX
        playbtn.y = centerY+400

          
        playbtn.touch = irParaTelaPartida
        playbtn:addEventListener( "touch", play )
       

    elseif ( phase == "did" ) then
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        playbtn:removeSelf()
        escalacaoImg:removeSelf()
    elseif ( phase == "did" ) then
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene