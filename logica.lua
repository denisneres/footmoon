local composer = require( "composer" )

display.setStatusBar(display.HiddenStatusBar)

centerX = display.contentWidth * .5
centerY = display.contentHeight * .5

bg = display.newImageRect("images/footmoon.jpg", 700,1100)
bg.x = centerX
bg.y = centerY

bgPartida = display.newImageRect("images/campo.jpg", 700,1100)
bgPartida.x = centerX
bgPartida.y = centerY
bgPartida.isVisible = false

logo = display.newImageRect("images/logo.png",500,300)
logo.x = centerX
logo.y = centerY-340

botaoSelecionarTimes = display.newRoundedRect(centerX, centerY-50, 400, 100,10 )
botaoSelecionarTimes:setFillColor(0.8,0.8,0)
TextoSelecionarTimes = display.newText("Selecionar Time", botaoSelecionarTimes.x, botaoSelecionarTimes.y, "gameFont")
TextoSelecionarTimes:setFillColor(0,0,0.8)

b1 = 10
b2 = 10
b3 = 20
b4 = 10
b5 = 10

e1 = 15
e2 = 15
e3 = 15
e4 = 15
e5 = 15

ForcaTimeB = (b1+b2+b3+b4+b5)/5
ForcaTimeA = (e1+e2+e3+e4+e5)/5
total = ForcaTimeA + ForcaTimeB

local secondsLeft = 120  -- 10 minutes * 60 seconds
local function updateTime( event )
 
    -- Decrement the number of seconds
    secondsLeft = secondsLeft - 1
 
    -- Time is tracked in seconds; convert it to minutes and seconds
    local minutes = math.floor( secondsLeft / 60 )
    local seconds = secondsLeft % 60
 
    -- Make it a formatted string
    local timeDisplay = string.format( "%02d:%02d", minutes, seconds )
    
    -- ataque time A
    math.randomseed(os.time())
    result = math.random(ForcaTimeA + 20*ForcaTimeB*(placarA.text+1))
    if result <= ForcaTimeA then
        placarA.text = placarA.text + 1
        narracaoTexto.text = "GOOOOOOOL DO BRASIL"
    end

    -- ataque time B
    math.randomseed(os.time())
    result = math.random(ForcaTimeB + 20*ForcaTimeA*(placarB.text+1))
    if result <= ForcaTimeB then
         placarB.text = placarB.text + 1
         narracaoTexto.text = "GOOOOOOOL DA FRANÇA"
    end
    -- Update the text object
    clockText.text = timeDisplay
    
end 

---------------------- TELA DE SELECIONAR O TIME ----------------------------------

local function telaSelecionarTime()


end

local function printTeamsLabels()

    clockText = display.newText( "02:00", centerX, centerY-200, native.systemFont, 72 )
    clockText:setFillColor( 0.7, 0.7, 1 )

    local teamA = display.newRoundedRect(centerX - 200, centerY, 200, 100,40 )
    teamA:setFillColor(0.8,0.8,0)
    local nameTeamA = display.newText("BRASIL", teamA.x, teamA.y, "gameFont")
    nameTeamA:setFillColor(0,0,0.8)

    local bgPlacarA = display.newRect(centerX - 50, centerY, 50, 50 )
    placarA = display.newText("0", bgPlacarA.x,bgPlacarA.y, "gameFont")
    placarA:setFillColor(0,0,0)
    local versusSimbolo = display.newText("X", centerX,centerY, "gameFont")
    
    local bgPlacarB = display.newRect(centerX + 50, centerY, 50, 50 )
    placarB = display.newText("0", bgPlacarB.x,bgPlacarB.y, "gameFont")
    placarB:setFillColor(0,0,0)
    
    local teamB = display.newRoundedRect(centerX + 200, centerY, 200, 100, 40 )
    teamB:setFillColor(0.1,0.2,0.5)
    local nameTeamB = display.newText("FRANÇA", teamB.x, teamB.y, "gameFont")
    nameTeamB:setFillColor(0.6,0,0)

    narracaoSpace = display.newRect(centerX, centerY + 300, 650, 150 )
    narracaoTexto = display.newText("Começa a partida entre Brasil e França!",narracaoSpace.x, narracaoSpace.y,"gameFont")
    narracaoTexto:setFillColor(0,0,0)

    local countDownTimer = timer.performWithDelay( 1000, updateTime, secondsLeft )
end

local function irParaTelaPartida( self, event )
    bg.isVisible = false
    bgPartida.isVisible = true
    botaoSelecionarTimes.isVisible = false
    logo.isVisible = false

    if ( event.phase == "began" ) then
        transition.to (logo, {delay = 500, time=1000, x = logo.x, y=-50, transition = easing.inOutQuad, onComplete=printTeamsLabels})

    end
    return true
end


botaoSelecionarTimes.touch = irParaTelaPartida
botaoSelecionarTimes:addEventListener( "touch", botaoSelecionarTimes )