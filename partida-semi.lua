local composer = require( "composer" )

local scene = composer.newScene()
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5

local secondsLeft = 10000  -- 10 minutes * 60 seconds
local segundosFaltando = 60
local chutesBrasil = 0
local chutesAdv = 0
local posseBolaBrasil = 0
local posseBolaAdv = 0
local jogadoresTitulares = {}
local arrayDecisaoJogada = {}
local estiloJogo = "normal"
local estiloJogoB = "normal"
local bolaComBrasil = true
local qntPassesBrasil = 0
local qntPassesAdv = 0
local tocou = true
local forcaDefesaBrasil = 30

gol = audio.loadStream( "sounds/gol.wav" )
apito = audio.loadStream( "sounds/apito.wav" )

function exibeResumoJogo()
    porcentPosseBrasil = posseBolaBrasil*100/(posseBolaAdv + posseBolaBrasil)
    porcentPosseAdv = 100 - porcentPosseBrasil
    print("Posse de bola: " .. " BRASIL " .. math.floor(porcentPosseBrasil) .. "%  x  " .. math.floor(porcentPosseAdv) .. "%  ITALIA ")
    print("Chutes a gol: " .. " BRASIL " .. chutesBrasil .. " x " .. chutesAdv .. " ITALIA ")
end

function atacar()
    if(estiloJogo == "ataque") then
        estiloJogo = "normal"
        btnAtaque.alpha = 0.5
        forcaDefesaBrasil = 30
    else
        estiloJogo = "ataque"
        btnDefesa.alpha = 0.5
        btnAtaque.alpha = 1.0
        forcaDefesaBrasil = 20
    end
end

function defender()
    if(estiloJogo == "defesa") then
        estiloJogo = "normal"
        btnDefesa.alpha = 0.5
        forcaDefesaBrasil = 30
        estiloJogoB = "normal"
    else
        estiloJogo = "defesa"
        estiloJogoB = "ataque"
        btnAtaque.alpha = 0.5
        btnDefesa.alpha = 1.0
        forcaDefesaBrasil = 40
    end
end

function montaArrayDecisaoJogada(qntPasses, estiloJogo)
    if (qntPasses == 0 and estiloJogo == "normal") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "P"
        arrayDecisaoJogada[5] = "P"
        arrayDecisaoJogada[6] = "P"
        arrayDecisaoJogada[7] = "D"
        arrayDecisaoJogada[8] = "D"
        arrayDecisaoJogada[9] = "D"
        arrayDecisaoJogada[10] = "C"
    
    elseif (qntPasses == 0 and estiloJogo == "ataque") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "P"
        arrayDecisaoJogada[5] = "D"
        arrayDecisaoJogada[6] = "D"
        arrayDecisaoJogada[7] = "D"
        arrayDecisaoJogada[8] = "D"
        arrayDecisaoJogada[9] = "D"
        arrayDecisaoJogada[10] = "C"
    
    elseif (estiloJogo == "defesa") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "P"
        arrayDecisaoJogada[5] = "P"
        arrayDecisaoJogada[6] = "P"
        arrayDecisaoJogada[7] = "P"
        arrayDecisaoJogada[8] = "P"
        arrayDecisaoJogada[9] = "D"
        arrayDecisaoJogada[10] = "C"

    elseif(qntPasses == 1 and estiloJogo=="normal") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "P"
        arrayDecisaoJogada[5] = "P"
        arrayDecisaoJogada[6] = "D"
        arrayDecisaoJogada[7] = "D"
        arrayDecisaoJogada[8] = "D"
        arrayDecisaoJogada[9] = "C"
        arrayDecisaoJogada[10] = "C"
    
    elseif(qntPasses == 1 and estiloJogo=="ataque") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "D"
        arrayDecisaoJogada[5] = "D"
        arrayDecisaoJogada[6] = "D"
        arrayDecisaoJogada[7] = "D"
        arrayDecisaoJogada[8] = "C"
        arrayDecisaoJogada[9] = "C"
        arrayDecisaoJogada[10] = "C"

    elseif(qntPasses >=2 and estiloJogo=="normal") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "P"
        arrayDecisaoJogada[4] = "P"
        arrayDecisaoJogada[5] = "D"
        arrayDecisaoJogada[6] = "D"
        arrayDecisaoJogada[7] = "C"
        arrayDecisaoJogada[8] = "C"
        arrayDecisaoJogada[9] = "C"
        arrayDecisaoJogada[10] = "C"
    
    elseif(qntPasses >=2 and estiloJogo=="ataque") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "D"
        arrayDecisaoJogada[4] = "D"
        arrayDecisaoJogada[5] = "D"
        arrayDecisaoJogada[6] = "C"
        arrayDecisaoJogada[7] = "C"
        arrayDecisaoJogada[8] = "C"
        arrayDecisaoJogada[9] = "C"
        arrayDecisaoJogada[10] = "C"
    
    elseif(estiloJogo == "superataque") then
        arrayDecisaoJogada[1] = "P"
        arrayDecisaoJogada[2] = "P"
        arrayDecisaoJogada[3] = "D"
        arrayDecisaoJogada[4] = "C"
        arrayDecisaoJogada[5] = "C"
        arrayDecisaoJogada[6] = "C"
        arrayDecisaoJogada[7] = "C"
        arrayDecisaoJogada[8] = "C"
        arrayDecisaoJogada[9] = "C"
        arrayDecisaoJogada[10] = "C"
    end
end

function jogadoresBrasilTitular()
    jogadoresTitulares = {}
    for index, jogador in ipairs(jogadores) do
        if(jogadores[index].titular) then
            table.insert(jogadoresTitulares,jogador)
        end
    end
end

local function playerTouched(event)

    if(jogadorSubs1 == nil) then
        jogadorSubs1 = event.target
        print("jogador 1 subs: ", jogadorSubs1)
    elseif(jogadorSubs2 == nil) then
        jogadorSubs2 = event.target
        print("jog 2 subs: ", jogadorSubs2)
    end
    
    if(jogadorSubs1 ~= nil and jogadorSubs2 ~= nil) then

        r = jogadorSubs1

        transition.to (jogadorSubs1, {time=300,x=jogadorSubs2.x, y=jogadorSubs2.y, alpha=1.0})
        transition.to (jogadorSubs2, {time=300,x=r.x, y=r.y, alpha=1.0})
        
        print(jogadorSubs1.titular)
        print(jogadorSubs2.titular)

        if(jogadorSubs1.titular ~= jogadorSubs2.titular) then
            if(jogadorSubs1.titular == false) then
                jogadorSubs1.titular = true
            else
                jogadorSubs1.titular = false
            end
            if(jogadorSubs2.titular == false) then
                jogadorSubs2.titular = true
            else
                jogadorSubs2.titular = false
            end
        end
        print(jogadorSubs1.titular)
        print(jogadorSubs2.titular)
        jogadorSubs1 = nil
        jogadorSubs2 = nil

    end        
end

local function clicadoParaSubs(event)
    jogadorSubs = event.target
end

function vaiParaTelaVitoria()
    composer.gotoScene( "vitoria-semi", {effect = "fade", time = "400"} )
end

function vaiParaTelaGameover()
    timer.cancel(timerGame)
    exibeResumoJogo()
    composer.gotoScene( "gameover", {effect = "fade", time = "400"} )
end

local function pararJogo(event)
    if(tempoRodando == true) then
        print(tempoRodando)
        tempoRodando = false
        adicionaBtnPlay()
    else

        print(tempoRodando)
        tempoRodando = true
        adicionaBtnPause()
    end
end

function adicionaBtnPlay()
    btnPause:removeSelf()
    btnPlay = display.newImage("images/btn_play.png")
    btnPlay.x = centerX
    btnPlay.y = centerY+400

    btnPlay.tap = pararJogo
    btnPlay:addEventListener( "tap", btnPlay )
end

function adicionaBtnPause()
    btnPlay:removeSelf()
    btnPause = display.newImage("images/btn_pause.png")
    btnPause.x = centerX
    btnPause.y = centerY+400

    btnPause.tap = pararJogo
    btnPause:addEventListener( "tap", btnPause )
end

function resumeJogo()
    
    pararJogo()
    bgSubs:removeSelf()
    botaoX:removeSelf()
    j1s:removeSelf()
    j2s:removeSelf()
    j3s:removeSelf()
    j4s:removeSelf()
    j5s:removeSelf()
    j6s:removeSelf()
    j7s:removeSelf()
    j8s:removeSelf()
    j9s:removeSelf()

end

function passaTempo()
    
end

function substituir()
    pararJogo()
    bgSubs = display.newImage("images/prancheta.png")
    bgSubs.x = centerX
    bgSubs.y = centerY

    j1s = display.newImage(jogadores[1].avatar)
    j1s.x = centerX
    j1s.y = centerY-100
    j1s:setFillColor( 0.5 )
    j1s:addEventListener( "touch", playerTouched )

    j2s = display.newImage(jogadores[2].avatar)
    j2s.x = centerX-150
    j2s.y = centerY-70
    j2s:setFillColor( 0.5 )
    j2s:addEventListener( "touch", playerTouched )

    j3s = display.newImage(jogadores[3].avatar)
    j3s.x = centerX+150
    j3s.y = centerY-70
    j3s:setFillColor( 0.5 )
    j3s:addEventListener( "touch", playerTouched )

    j4s = display.newImage(jogadores[4].avatar)
    j4s.x = centerX+150
    j4s.y = centerY+100
    j4s:setFillColor( 0.5 )
    j4s:addEventListener( "touch", playerTouched )

    j5s = display.newImage(jogadores[5].avatar)
    j5s.x = centerX+220
    j5s.y = centerY+300
    j5s:setFillColor( 0.5 )
    j5s:addEventListener( "touch", playerTouched )

    j6s = display.newImage(jogadores[6].avatar)
    j6s.x = centerX+70
    j6s.y = centerY+300
    j6s:setFillColor( 0.5 )
    j6s:addEventListener( "touch", playerTouched )

    j7s = display.newImage(jogadores[7].avatar)
    j7s.x = centerX-80
    j7s.y = centerY+300
    j7s:setFillColor( 0.5 )
    j7s:addEventListener( "touch", playerTouched )
    j8s = display.newImage(jogadores[8].avatar)
    j8s.x = centerX-230
    j8s.y = centerY+300
    j8s:setFillColor( 0.5 )
    j8s:addEventListener( "touch", playerTouched )

    j9s = display.newImage(jogadores[9].avatar)
    j9s.x = centerX-150
    j9s.y = centerY+100
    j9s:setFillColor( 0.5 )
    j9s:addEventListener( "touch", playerTouched )

    botaoX = display.newText( "X", 100, 200, native.systemFont, 26 )
    botaoX:setFillColor( 0, 0, 0 )

    botaoX.tap = resumeJogo
    botaoX:addEventListener( "tap", myText )

end

function updateTime()
    --audio.start(backgroundMusic)
    -- audio.stop(gol)
    if(tempoRodando == true) then
        segundosFaltando = segundosFaltando - 1
        local minutes = math.floor( segundosFaltando / 60 )
        local seconds = segundosFaltando % 60
    
        timeDisplay = string.format( "%02d:%02d", minutes, seconds )
        
        if (bolaComBrasil == true) then
            narracao:setFillColor(1.0,1.0,1.0)
            print("BOLA COM BRASIL")
           
            if(placarB.text < placarA.text) then
                estiloJogoB = "ataque"
            else
                estiloJogoB = "normal"
            end
            
            posseBolaBrasil = posseBolaBrasil + 1
            if( tocou == true) then
                jogadorAtacando = jogadoresTitulares[ math.random( #jogadoresTitulares )]
            end

            decisao = math.random(10)
            montaArrayDecisaoJogada(qntPassesBrasil,estiloJogo)
            print(estiloJogo)
            print("numero passes:" .. qntPassesBrasil .. " array: ")
            
            print("decisao:", decisao)
                
            -- JOGADOR ESCOLHE CHUTAR AO GOL
            if(arrayDecisaoJogada[decisao] == 'C') then
                print("CHUTOU")
                
                -- narracao.text = narracao.text .. "\n" .. jogadorAtacando.nome .. " chutou..."
                bolaComBrasil = false
                tocou = true  -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                qntPassesBrasil = 0
                chutesBrasil = chutesBrasil + 1
                resultadoChute = math.random(100)
                -- LOGICA CHUTE DO JOGADOR SE VAI OU NAO NA DIRECAO DO GOL
                if(resultadoChute <= 50) then
                    -- LOGICA SE GOLEIRO PEGA OU NAO O CHUTE
                    if(math.random(100) > 30 ) then
                        audio.play(gol)
                        narracao.text = "GOOOOOOOL DO " .. jogadorAtacando.nome .. " !!!!"
                        placarA.text = placarA.text + 1
                      
                    else
                        narracao.text = jogadorAtacando.nome .. " chutou e goleiro pegou."
                    end
                else
                    narracao.text = jogadorAtacando.nome .. " chutou pra fora."

                end
                   
            
            -- JOGADOR ESCOLHE DRIBLAR O ADVERSARIO
            elseif(arrayDecisaoJogada[decisao] == 'D') then
                
                possibilidadeDriblar = jogadorAtacando.ataque * 2
                possibilidadeDriblarErr = 10

                possiblidadeFinal = math.random(possibilidadeDriblar + possibilidadeDriblarErr);

                print("tentou driblar ")
                if(possiblidadeFinal <= possibilidadeDriblar) then
                    print("driblooou")
                    --narracao.text = "\n" ..  narracao.text .. "\n" .. jogadorAtacando.nome .. " driblou um adversario"
                    posseBolaBrasil = posseBolaBrasil + 1
                    tocou = false
                    bolaComBrasil = true
                    narracao.text = jogadorAtacando.nome ..  " driblou!"
                else
                    narracao.text = jogadorAtacando.nome .. " errou drible. Bola da ITALIA."
                
                    bolaComBrasil = false
                    tocou = true  -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                    qntPassesBrasil = 0
                end
            

            elseif(arrayDecisaoJogada[decisao] == 'P') then
                print("TOCOU")
                possibilidadeTocar = jogadorAtacando.ataque * 2
                possibilidadeTocarErr = 10

                possiblidadeFinal = math.random(possibilidadeTocar + possibilidadeTocarErr);
                
                

                if(possiblidadeFinal <= possibilidadeTocar) then
                    narracao.text = jogadorAtacando.nome ..  " tocou a bola."
                    qntPassesBrasil = qntPassesBrasil + 1
                    tocou = true
                    bolaComBrasil = true
                        
                else
                    bolaComBrasil = false
                    tocou = true -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                    narracao:setFillColor(1.0,1.0,0.0)
                    narracao.text =  jogadorAtacando.nome .. " errou. Bola da ITALIA."
                    qntPassesBrasil = 0
                end
                    
                    --narracao.text = narracao.text .. "\n" .. jogadorAtacando.nome .. " passa a bola para companheiro"
            end
            
            
            -- Update the text object
            clockText.text = timeDisplay

        
        else
            print("BOLA COM ADVERSARIO")
            narracao:setFillColor(1.0,1.0,0.0)
             -- ataque time B
              
            if(tocou == true) then
                advAtacando = jogadoresAdversario[ math.random( #jogadoresAdversario )]
               
            end
            
            montaArrayDecisaoJogada(qntPassesAdv, estiloJogoB)
            print(estiloJogoB)
            
            decisao = math.random(10)
            print("decisao:", decisao)
                 
            -- JOGADOR ESCOLHE CHUTAR AO GOL
            if(arrayDecisaoJogada[decisao] == 'C') then
                print("CHUTOU")
                --narracao.text = narracao.text .. "\n" .. advAtacando.nome .. " chutou..."
                bolaComBrasil = true
                tocou = true  -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                qntPassesAdv = 0
                chutesAdv = chutesAdv + 1
                resultadoChute = math.random(100)
                    
                if(resultadoChute <= advAtacando.ataque/2) then
                    --narracao.text = narracao.text .. "\n" .. "olha o gol..."
                    if(math.random(100) > forcaDefesaBrasil ) then
                        narracao.text = "GOL DO " .. advAtacando.nome .. "."
                        placarB.text = placarB.text + 1
                    else
                        narracao.text = advAtacando.nome .. " chutou e goleiro pegou."
                        
                    end
                    
                else
                    narracao.text = advAtacando.nome .. " chutou pra fora."
                end
                         
            -- JOGADOR ESCOLHE DRIBLAR O ADVERSARIO
            elseif(arrayDecisaoJogada[decisao] == 'D') then
                possibilidadeDriblar = advAtacando.ataque * 2
                possibilidadeDriblarErr = 20

                possiblidadeFinal = math.random(possibilidadeDriblar + possibilidadeDriblarErr);

                print("tentou driblar ")
                if(possiblidadeFinal <= possibilidadeDriblar) then
                    print("driblooou")
                    posseBolaAdv = posseBolaAdv + 1
                    tocou = false
                    bolaComBrasil = false
                    narracao.text = advAtacando.nome ..  " driblou!"
                else
                    narracao.text = advAtacando.nome .. " errou drible. Bola do Brasil."
                    bolaComBrasil = true
                    tocou = true  -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                    qntPassesAdv = 0
                end
                     
            -- JOGADOR PASSA A BOLA
            elseif(arrayDecisaoJogada[decisao] == 'P') then
                print("TOCOU")
                
                possibilidadeTocar = jogadorAtacando.ataque * 2
                possibilidadeTocarErr = 10

                possiblidadeFinal = math.random(possibilidadeTocar + possibilidadeTocarErr);
                
                
                if(possiblidadeFinal <= possibilidadeTocar) then
                    narracao.text = advAtacando.nome ..  " tocou a bola."
                    qntPassesAdv = qntPassesAdv + 1
                    tocou = true
                    bolaComBrasil = false
                    
                else
                    bolaComBrasil = true
                    tocou = true  -- esta true para que na vez do adv ele possa sortear quem esta com a bola.
                    qntPassesAdv = 0
                    narracao:setFillColor(1.0,1.0,1.0)
                    narracao.text = advAtacando.nome .. " errou. Bola do Brasil."
                end
                    --narracao.text = narracao.text .. "\n" .. jogadorAtacando.nome .. " passa a bola para companheiro"
            end

            -- Update the text object
            clockText.text = timeDisplay
        end
    end
    if(segundosFaltando == 0) then
        if(placarA.text > placarB.text) then
            vaiParaTelaVitoria()
        else
            vaiParaTelaGameover()
        end
    end

end

function printTeamsLabels()
    
    clockText = display.newText( "01:00", centerX, centerY-190, native.systemFont, 72 )
    clockText:setFillColor( 0.0, 0.0, 0.0 )


    local bgPlacarA = display.newRect(centerX - 45, centerY+65, 50, 50 )
    bgPlacarA:setFillColor(0.9,0.9,0.9)
    placarA = display.newText("0", bgPlacarA.x,bgPlacarA.y, "gameFont")
    placarA:setFillColor(0,0,0)
    
    local bgPlacarB = display.newRect(centerX + 60, centerY+65, 50, 50 )
    bgPlacarB:setFillColor(0.9,0.9,0.9)
    placarB = display.newText("0", bgPlacarB.x,bgPlacarB.y, "gameFont")
    placarB:setFillColor(0,0,0)
    

    narracao = display.newText( "Começa o jogo", centerX, centerY+230)
    narracao:setFillColor(1,1,1)

    math.randomseed(os.time())
    
    jogadoresBrasilTitular()

    timerGame = timer.performWithDelay( 1000, updateTime, secondsLeft )
   

    fimjogo = audio.loadStream( "sounds/fimdojogo.wav" )
    bgfimjogo = audio.play( fimjogo )

end


function scene:create( event )
    local sceneGroup = self.view
    audio.play(apito)
end
 
function scene:show( event )

    jogadores = event.params.jogadores

    local sceneGroup = self.view
    local phase = event.phase
    tempoRodando = true

    if ( phase == "will" ) then


        bgPartida = display.newImage("images/partida_semi.png")
        bgPartida.x = centerX
        bgPartida.y = centerY
        
        btnPause = display.newImage("images/btn_pause.png")
        btnPause.x = centerX
        btnPause.y = centerY+400

        btnAtaque = display.newImage("images/btn_ataque.png")
        btnAtaque.x = centerX+200
        btnAtaque.y = centerY+400
        btnAtaque.alpha = 0.5


        btnDefesa = display.newImage("images/btn_defesa.png")
        btnDefesa.x = centerX-200
        btnDefesa.y = centerY+400
        btnDefesa.alpha = 0.5

        forcaTimeA = 0
        forcaTimeADef = 0
        forcaTimeB = 0
        forcaTimeBDef = 0

        -- MONTA JOGADORES E FORCAS TIME BRASIL
        for k,jogador in ipairs(jogadores) do
            forcaTimeA = forcaTimeA + jogador.ataque
            forcaTimeADef = forcaTimeADef + jogador.defesa  
        end

        -- MONTA JOGADORES E FORÇAS TIME ADVERSARIO
        jogadoresAdversario = {}
       
        player1 = { ataque=7, defesa=50, nome="Dante" }
        table.insert(jogadoresAdversario, player1)

        player2 = { ataque=9, defesa=9, nome="Mario Bros" }
        table.insert(jogadoresAdversario, player2)
        
        player3 = { ataque=8, defesa=7, nome="Da Vinci" }
        table.insert(jogadoresAdversario, player3)

        player4 = { ataque=10, defesa=9, nome="Donatelo" }
        table.insert(jogadoresAdversario, player4)

        player5 = { ataque=10, defesa=9, nome="Caravagio" }
        table.insert(jogadoresAdversario, player5)

        player6 = { ataque=5, defesa=9, nome="Michelangelo" }
        table.insert(jogadoresAdversario, player6)

        for k,j in ipairs(jogadoresAdversario) do
            forcaTimeB = forcaTimeB + j.ataque
            forcaTimeBDef = forcaTimeBDef + j.defesa  
        end

        
        btnPause.tap = pararJogo
        btnPause:addEventListener( "tap", btnPause )

        btnAtaque.tap = atacar
        btnAtaque:addEventListener( "tap", btnAtaque )

        btnDefesa.tap = defender
        btnDefesa:addEventListener( "tap", btnDefesa )

      --  btnSubstituir.tap = substituir
      --  btnSubstituir:addEventListener( "tap", btnSubstituir )
        
        

        printTeamsLabels()
        
        
    elseif ( phase == "did" ) then
 
    end
end


 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then

        audio.stop( { channel=2 })

        timer.cancel(timerGame)
        exibeResumoJogo()
        btnPause:removeSelf()
        btnAtaque:removeSelf()
        btnDefesa:removeSelf()
        clockText:removeSelf()
        placarA:removeSelf()
        
        placarB:removeSelf()
        bgPartida:removeSelf()
        narracao:removeSelf()
       
        

    elseif ( phase == "did" ) then
 
    end
end
 
 
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene



