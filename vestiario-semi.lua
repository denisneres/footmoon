local composer = require( "composer" )

local scene = composer.newScene()

local function playerTouched(event)
    print(jogadorSubs1)
    print(jogadorSubs2)
    if(jogadorSubs1 == nil) then
        jogadorSubs1 = event.target
        print("jogador 1 subs: ", jogadorSubs1)
    elseif(jogadorSubs2 == nil) then
        jogadorSubs2 = event.target
        print("jog 2 subs: ", jogadorSubs2)
    end
    
    if(jogadorSubs1 ~= nil and jogadorSubs2 ~= nil) then

        r = jogadorSubs1

        transition.to (jogadorSubs1, {time=300,x=jogadorSubs2.x, y=jogadorSubs2.y, alpha=1.0})
        transition.to (jogadorSubs2, {time=300,x=r.x, y=r.y, alpha=1.0})
        
        print(jogadorSubs1.titular)
        print(jogadorSubs2.titular)

        if(jogadorSubs1.titular ~= jogadorSubs2.titular) then
            if(jogadorSubs1.titular == false) then
                jogadorSubs1.titular = true
            else
                jogadorSubs1.titular = false
            end
            if(jogadorSubs2.titular == false) then
                jogadorSubs2.titular = true
            else
                jogadorSubs2.titular = false
            end
        end
        print(jogadorSubs1.titular)
        print(jogadorSubs2.titular)
        jogadorSubs1 = nil
        jogadorSubs2 = nil

    end        
end

local function clicadoParaSubs(event)
    jogadorSubs = event.target
end

local function irParaTelaPartida( self, event )
    irParaTelaExibicao()
end



function irParaTelaExibicao()
    composer.gotoScene( "escalacaoadv_semi", {effect = "fade", time = "100", params={jogadores = jogadores } } )
end  

function scene:create( event )
 
    local sceneGroup = self.view
end
 
function somadorForcasAtaqueEDefesa()
    totalAtaque = 0
    totalDefesa = 0

end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        telaVestiario = display.newImage("images/vestiario_semi.png")
        telaVestiario.x = centerX
        telaVestiario.y = centerY
        
        btnIrCampo = display.newImage("images/btn_ircampo.png")
        btnIrCampo.x = centerX
        btnIrCampo.y = centerY+410

        jogadores = {}
        titulares = {}
        reservas = {}

        p1 = display.newImage("images/tim.png")
        p1.avatar = "images/tim.png"
        p1.nome = "Tim"
        p1.x = centerX
        p1.y = centerY+400
        p1.titular = true
        p1.ataque = 15
        p1.defesa = 85
        p1:setFillColor( 0.5 )
        p1:addEventListener( "tap", playerTouched )

        transition.to( p1, { time=1000, alpha=1.0, x=centerX, y=centerY-100} )

        table.insert(jogadores, p1)

        p2 = display.newImage("images/zecanew.png")
        p2.avatar = "images/zecanew.png";
        p2.nome = "Zeca"
        p2.x = centerX
        p2.y = centerY+400
        p2.ataque = 15
        p2.defesa = 80
        p2:setFillColor( 0.5 )
        p2.titular = true
        p2:addEventListener( "tap", playerTouched )

        transition.to( p2, { time=1000, alpha=1.0, x=centerX-150, y=centerY-70} )

        table.insert(jogadores, p2)

        p3 = display.newImage("images/wesley.png")
        p3.avatar = "images/wesley.png"
        p3.nome = "Wesley"
        p3.x = centerX
        p3.y = centerY+400
        p3.ataque = 90
        p3.defesa = 20
        p3.titular = true
        p3:setFillColor( 0.5 )
        p3:addEventListener( "tap", playerTouched )

        transition.to( p3, { time=1300, alpha=1.0, x=centerX+150, y=centerY-70} )

        table.insert(jogadores, p3)

        p4 = display.newImage("images/milton.png")
        p4.avatar = "images/milton.png"
        p4.nome = "Milton"
        p4.x = centerX
        p4.y = centerY+400
        p4.ataque = 75
        p4.defesa = 45
        p4:setFillColor( 0.5 )
        p4.titular = true
        p4:addEventListener( "tap", playerTouched )

        transition.to( p4, { time=1000, alpha=1.0, x=centerX-150, y=centerY+100} )

        table.insert(jogadores, p4)

        p5 = display.newImage("images/gil.png")
        p5.avatar = "images/gil.png"
        p5.nome = "Gil"
        p5.x = centerX
        p5.y = centerY+400
        p5.ataque = 80
        p5.defesa = 20
        p5.titular = true
        p5:setFillColor( 0.5 )
        p5:addEventListener( "tap", playerTouched )

        transition.to( p5, { time=1000, alpha=1.0, x=centerX+150, y=centerY+100} )
        
        table.insert(jogadores, p5)
        
        p6 = display.newImage("images/tom.png")
        p6.avatar = "images/tom.png"
        p6.nome = "Tom"
        p6.x = centerX+220
        p6.y = centerY+300
        p6.ataque = 20
        p6.defesa = 80
        p6.titular = false
        p6:setFillColor( 1.0 )
        p6:addEventListener( "tap", playerTouched )

        table.insert(jogadores, p6)

        p7 = display.newImage("images/zeze.png")
        p7.avatar = "images/zeze.png"
        p7.nome = "Zeze"
        p7.x = centerX+70
        p7.y = centerY+300
        p7.ataque = 75
        p7.defesa = 25
        p7.titular = false
        p7:setFillColor( 1.0 )
        p7:addEventListener( "tap", playerTouched )

        table.insert(jogadores, p7)

        p8 = display.newImage("images/roberto.png")
        p8.avatar = "images/roberto.png"
        p8.nome = "Roberto"
        p8.x = centerX-80
        p8.y = centerY+300
        p8.ataque = 40
        p8.defesa = 60
        p8.titular = false
        p8:setFillColor( 1.0 )
        p8:addEventListener( "tap", playerTouched )
        
        table.insert(jogadores, p8)

        p9 = display.newImage("images/luiz.png")
        p9.avatar = "images/luiz.png"
        p9.nome = "Luiz"
        p9.x = centerX-230
        p9.y = centerY+300
        p9.ataque = 30
        p9.defesa = 75
        p9.titular = false
        p9:setFillColor( 1.0 )
        p9:addEventListener( "tap", playerTouched )

        table.insert(jogadores, p9)

         btnIrCampo.tap = irParaTelaPartida
         btnIrCampo:addEventListener( "tap", btnIrCampo )

    elseif ( phase == "did" ) then
 
    end
end

function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        p1:removeSelf()
        p2:removeSelf()
        p3:removeSelf()
        p4:removeSelf()
        p5:removeSelf()
        p6:removeSelf()
        p7:removeSelf()
        p8:removeSelf()
        p9:removeSelf()
        btnIrCampo:removeSelf()
        telaVestiario:removeSelf()
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
 
end
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene