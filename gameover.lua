local composer = require( "composer" )
 
local scene = composer.newScene()
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5

gameover = audio.loadStream( "sounds/gameover.wav" )


local function irParaTelaPartida( self, event )
    audio.play(click)
        irParaTelaSelecionarTimes()
end
    
function irParaTelaSelecionarTimes()
        composer.gotoScene( "menu", {effect = "fade", time = "400"} )
end  

function scene:create( event )
    
    local sceneGroup = self.view
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
 
    if ( phase == "will" ) then
        
        bg = display.newImage("images/gameover.png")
        bg.x = centerX
        bg.y = centerY
 
        btIrMenu = display.newImage("images/gameover_btn.png")
        btIrMenu.x = centerX
        btIrMenu.y = centerY+400

        audio.play(gameover)

        btIrMenu.touch = irParaTelaPartida
        btIrMenu:addEventListener( "touch", btIrMenu )
       
    elseif ( phase == "did" ) then
       
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        bg:removeSelf()
        btIrMenu:removeSelf()
 
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene