local composer = require( "composer" )
 
local scene = composer.newScene()
local bg = display.newImage("images/instrucoes.png")
local btComecar = display.newImage("images/botao_start.png")
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5
click = audio.loadSound( "sounds/click.wav" )

function selecionarTime(event)

end

options = {
    effect = "flip",
    time = 1000
}

local function irParaTelaPartida( self, event )
    audio.reserveChannels( 2 )
    audio.setVolume(1.0 , { channel=2 } )
    audio.play(click)
    irParaTelaSelecionarTimes()
end
    
function irParaTelaSelecionarTimes()
        composer.gotoScene( "exibicaoOitavas", {effect = "fade", time = "400"} )
end  

function scene:create( event )
    local sceneGroup = self.view
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
 
    if ( phase == "will" ) then

        bg.x = centerX
        bg.y = centerY

        btComecar.x = centerX-500
        btComecar.y = centerY-350

        btComecar.touch = irParaTelaPartida
        btComecar:addEventListener( "touch", btComecar )
        
        transition.to( btComecar, { time=1000, alpha=1.0, x=centerX+250, y=centerY-350} )

    elseif ( phase == "did" ) then
        
        audio.play( music, { channel=1, loops=-1 } )
       
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        bg:removeSelf()
        btComecar:removeSelf()
        
 
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view

end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene