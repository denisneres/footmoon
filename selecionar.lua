local composer = require( "composer" )
 
local scene = composer.newScene()
centerX = display.contentWidth * .5
centerY = display.contentHeight * .5 


function brasilSelecionado()
    composer.gotoScene("prejogo")
end

function scene:create( event )
 
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
 
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        bgPartida = display.newImageRect("images/newbg.png", 700,1100)
        bgPartida.x = centerX
        bgPartida.y = centerY
        bgPartida.isVisible = true

        local myText = display.newText( "Selecione seu Time", centerX, centerY-300,"gameFont", 60 )
        myText:setFillColor( 1, 1, 1 )

        banBrasil = display.newImageRect("images/flags/br.png", 150,100)
        banBrasil.x = centerX-200
        banBrasil.y = centerY-150
        banBrasil.isVisible = true

        banFranca = display.newImageRect("images/flags/fr.png", 150,100)
        banFranca.x = centerX
        banFranca.y = centerY-150
        banFranca.isVisible = true

        banAlemanha = display.newImageRect("images/flags/de.png", 150,100)
        banAlemanha.x = centerX+200
        banAlemanha.y = centerY-150
        banAlemanha.isVisible = true

        banArg = display.newImageRect("images/flags/ar.png", 150,100)
        banArg.x = centerX-200
        banArg.y = centerY
        banArg.isVisible = true

        banItalia = display.newImageRect("images/flags/it.png", 150,100)
        banItalia.x = centerX
        banItalia.y = centerY
        banItalia.isVisible = true

        banEspanha = display.newImageRect("images/flags/es.png", 150,100)
        banEspanha.x = centerX+200
        banEspanha.y = centerY
        banEspanha.isVisible = true

        banMexico = display.newImageRect("images/flags/mx.png", 150,100)
        banMexico.x = centerX-200
        banMexico.y = centerY+150
        banMexico.isVisible = true

        banBelgica = display.newImageRect("images/flags/be.png", 150,100)
        banBelgica.x = centerX
        banBelgica.y = centerY+150
        banBelgica.isVisible = true

        banJapao = display.newImageRect("images/flags/jp.png", 150,100)
        banJapao.x = centerX+200
        banJapao.y = centerY+150
        banJapao.isVisible = true

       
        banBrasil.touch = brasilSelecionado
        banBrasil:addEventListener( "touch", banBrasil )
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene